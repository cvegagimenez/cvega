---
title: "Smart Garden"
date: 2020-01-15 9:00:00
categories: [projects]
tags: [MSP432, iot, arduino, grafana, project]

---

![pipeline](https://gitlab.com/Gigigotrek/smartgarden/badges/master/pipeline.svg?key_text=smartgarden&key_width=95)

As a project at the University I had to develop an smart garden to allow to monitorize the status of a urban garden. To do so we used an MSP432P401R where the different sensors were soldered and a wireless board to allow internet connectivity. 

All the metrics are presented by a Grafana dashboard that also allows to activate manually or automatically the water pump. We used MQTT to send the metrics and insert it to an InfluxDB configured as datasouce in Grafana. All the services are running over Docker Swarm in an VPS to orchestrate the different containers used.

[Check it out](https://gitlab.com/Gigigotrek/smartgarden) here.
