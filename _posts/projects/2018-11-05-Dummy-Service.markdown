---
title: "Dummy Service"
date: 2018-11-05 22:10:00
categories: [projects]
tags: [python, dummy, flask, project]

---

![pipeline](https://gitlab.com/Gigigotrek/dummy_service/badges/master/pipeline.svg?key_text=dummy-service&key_width=95)

A Dummy Service developed in Python3. It runs a Flask App and the idea is to use it as a playground to test functionalities and processes. This app is running in a python alpine docker container and is deployed using docker compose. To make the tests easier, I am using a Continuous Deployment cycle provided by the free service of Gitlab-CI. 

As a playground, the idea is to work iteratively in the project.


[Check it out](https://gitlab.com/Gigigotrek/dummy_service/) here.
